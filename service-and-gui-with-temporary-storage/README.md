# LT service + restserver + temporary storage, plus trial GUI

This stack runs

- an LT service container
- the temporary storage service
- the ELG restserver to translate the public to internal APIs
- a trial GUI such as the standard [gui-ie](https://gitlab.com/european-language-grid/usfd/gui-ie)

The compose file expects certain configuration options in an environment file which must be named `.env` (or specified with the `--env-file` option to `docker-compose`) - copy the `template.env` file as a starting point.

