# Local docker-compose stacks for LT services

This repository provides a selection of `docker-compose` stack definitions to support running ELG-compliant LT services outside of the main ELG cluster.  There are several reasons why you may want to do this, including:

- local testing of a new LT service or custom "try out" GUI you are developing, before you submit it to the ELG
- you want to use an open-source ELG service to process more data than your quota on the main platform would allow
- you want to use an open-source ELG service to process data that you cannot send over the internet for privacy or regulatory reasons (e.g. medical data)

The public API offered by the ELG platform and the internal API exposed by the LT services themselves are related but not identical, so while you can simply run an LT service docker image alone and interact with that using the internal API, if you want to use the same client code locally as you would use against the public ELG then you will also need to run the "restserver" component, which translates between the public and internal APIs.  And if the service you are using requires the temporary storage service `http://storage.elg/store` then you will need to run that as well.

There are several folders with stacks that cover different scenarios.
