# LT service + restserver + temporary storage, no GUI

This stack is suitable for a single LT service container that depends on the temporary storage service at `http://storage.elg/store`, plus the ELG restserver to translate the public to internal APIs.  The compose file expects certain configuration options in an environment file which must be named `.env` (or specified with the `--env-file` option to `docker-compose`) - copy the `template.env` file as a starting point.

The stack does not include a trial GUI, you will need to call the service via the standard ELG public API.

